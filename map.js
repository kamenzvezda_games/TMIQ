class MapObject {
    constructor(x, y, spriteName, passability) {
        this.x = x;
        this.y = y;
        this.spriteName = spriteName;
        this.passable = passability;
        this.sprite;
    }
}

function Random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Map {
    constructor() { }

    fillRectangle(map, rect, symbol) {
        for (var i = rect.y; i < rect.y + rect.height; i++) {
            for (var j = rect.x; j < rect.x + rect.width; j++) {
                map[i][j] = symbol;
            }
        }
    }

    checkSurroundedTiles(map, y, x, mainType, secondaryType) {
        var tileCount = 0;
        for (var dx = -1; dx <= 1; dx++) {
            for (var dy = -1; dy <= 1; dy++) {
                if (game.isInBorders(x + dx, y + dy) && map[y + dy][x + dx] === mainType)
                    tileCount++;
            }
        }
        return tileCount < 5;
    }

    getRandomAvailablePos(maxWidth, availablePositions) {
        var pos = Math.floor(Math.random() * availablePositions.length);
        var element = availablePositions[pos];
        while (element.x > maxWidth - 1) {
            pos = Math.floor(Math.random() * availablePositions.length);
            element = availablePositions[pos];
        }
        availablePositions.splice(pos, 1);
        return element;
    }
    
    copyObjectsToMap(width, height, sourceMap, targetMap, secondaryType, passability) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                if (sourceMap[y][x].spriteName.replace(/[0-9]/g, '') != secondaryType) {
                    targetMap[y][x].sprite = phaserGame.add.sprite(x * SPRITE_SIZE, y * SPRITE_SIZE, sourceMap[y][x].spriteName);
                    targetMap[y][x].sprite.autoCull = true;
                    targetMap[y][x].passable = passability;
                }
            }
        }
    }

    convertToTileMap(map, width, height, setSprite, passability, previousMaps, secondaryType) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                this.setRandomObject(y, x, map, map[y][x].toString(), passability, previousMaps, secondaryType);
                if (setSprite) {
                    map[y][x].sprite = phaserGame.add.sprite(x * SPRITE_SIZE, y * SPRITE_SIZE, map[y][x].spriteName);
                    map[y][x].sprite.autoCull = true;
                }
            }
        }
    }

    makeRarerTileApperance(y, x, objectName, objectQuantity, previousMaps, secondaryType) {
        var rareTiles = ["Grass5", "Grass6", "Grass7", "Grass8", "Ground7", "Ground8", "Masonry5"];
        var objectNum = Math.floor(Math.random() * objectQuantity + 1);
        if (objectName === "Grass" || objectName === "Masonry" && rareTiles.includes(objectName + objectNum)) {
            for (var i = 0; i < previousMaps.length; i++) {
                if (previousMaps[i][y][x].toString().replace(/^[1-9]+$/, "") != secondaryType)
                    return Math.floor(Math.random() * 4 + 1);
            }
        }
        if (rareTiles.includes(objectName + objectNum)) {
            if (Math.random() < 0.9)
                return Math.floor(Math.random() * 3 + 1);
        }
        return objectNum;
    }

    checkOnWarpPosition(y, x, map) {
        return ((x === map.warpPos.x && (map.warpPos.y === "any" || y === map.warpPos.y)));
    }

    setRandomObject(y, x, map, objectName, passability, previousMaps, secondaryType) {
        var objectQuantity = 0;
        while (phaserGame.cache.checkImageKey(objectName + (objectQuantity + 1).toString()))
            objectQuantity++;
        var objectNum = this.makeRarerTileApperance(y, x, objectName, objectQuantity, previousMaps, secondaryType);
        map[y][x] = new MapObject(x, y, objectName + objectNum, passability);
    }

    checkOnStandaloneTile(map, y, x, mainType, secondaryType, name, oldName) {
        if (!game.isInBorders(x, y - 1)) {
            if (map[y][x - 1] === secondaryType && map[y][x + 1] === secondaryType)
                return oldName + "RightLeftUp";
        }
        if (!game.isInBorders(x, y + 1)) {
            if (map[y][x - 1] === secondaryType && map[y][x + 1] === secondaryType)
                return oldName + "RightLeftDown";
        }
        if (!game.isInBorders(x + 1, y)) {
            if (map[y + 1][x] === secondaryType && map[y - 1][x] === secondaryType)
                return oldName + "RightDownUp";
        }
        if (!game.isInBorders(x - 1, y)) {
            if (map[y + 1][x] === secondaryType && map[y - 1][x] === secondaryType)
                return oldName + "LeftDownUp";
        }
        return name;
    }

    chooseTransferTile(map, y, x, mainType, secondaryType) {
        var name = (mainType + secondaryType).toString();
        if (mainType != "Ground")
            name = mainType.toString();
        var oldName = name;
        if (this.checkOnSecondaryTile(map, y, x + 1, secondaryType))
            name += "Right";
        if (this.checkOnSecondaryTile(map, y, x - 1, secondaryType))
            name += "Left";
        if (this.checkOnSecondaryTile(map, y + 1, x, secondaryType))
            name += "Down";
        if (this.checkOnSecondaryTile(map, y - 1, x, secondaryType))
            name += "Up";
        if (name === oldName && mainType === "Ground") {
            name = (secondaryType + mainType).toString();
            oldName = name;
            if (this.checkOnSecondaryTile(map, y - 1, x - 1, secondaryType))
                name += "RightDown";
            if (this.checkOnSecondaryTile(map, y + 1, x - 1, secondaryType))
                name += "RightUp";
            if (this.checkOnSecondaryTile(map, y - 1, x + 1, secondaryType))
                name += "LeftDown";
            if (this.checkOnSecondaryTile(map, y + 1, x + 1, secondaryType))
                name += "LeftUp";
        }
        if (name === oldName || !phaserGame.cache.checkImageKey(name + "1"))
            return mainType;
        return this.checkOnStandaloneTile(map, y, x, mainType, secondaryType, name, oldName);
    }

    placeTransferTiles(map, mainType, secondaryType, width, height) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                if (map[y][x] === mainType)
                    map[y][x] = this.chooseTransferTile(map, y, x, mainType, secondaryType);
            }
        }
    }

    checkOnSecondaryTile(map, y, x, secondaryType) {
        return (game.isInBorders(x, y) && map[y][x] === secondaryType);
    }
}