class Effect {
    constructor(stats) {
        this.duration = 2;
        this.desc = "Some Effect";
    }

    affect(actor) {
        //console.log("Affecting " + actor.name);
        actor.hp--;
    }
}

class BurningEffect extends Effect {
    constructor(stats) {
        super();
        // this.duration = Math.floor(stats.int + stats.con / 2);
        this.duration = 2;

        this.dmg = stats.int;
        this.desc = "Burning";
    }

    affect(actor) {
        //console.log("Affecting " + actor.name + " with " + this.desc);
        if (actor.effects[this.desc].duration <= 0) return;
        actor.stats.hp -= this.dmg;
        actor.effects[this.desc].duration--;
    }
}

class BleedEffect extends Effect {
    constructor(stats) {
        super();
        // this.duration = Math.floor(stats.int + stats.con / 2);
        this.duration = 5;
        this.dmg = stats.int;
        this.desc = "Bleed";
    }

    affect(actor) {
        //console.log("Affecting " + actor.name + " with " + this.desc);
        if (actor.effects[this.desc].duration <= 0) return;
        actor.stats.hp -= this.dmg;
        actor.effects[this.desc].duration--;
    }
}

class SlowingEffect extends Effect {
    constructor(stats) {
        super();
        this.duration = Math.floor(stats.int + stats.con / 4);
        this.defaultSpeed = -1;
        this.defaultEnergy = -1;

        this.desc = "Slowing";
    }

    affect(actor) {
        if (actor.effects[this.desc].duration <= 0) {
            actor.stats.speed = this.defaultSpeed;
            actor.stats.energy = this.defaultEnergy;
            return;
        }
        //console.log("Affecting " + actor.name + " with " + this.desc);
        //Инициализируем только один раз
        if (this.defaultSpeed == -1 && this.defaultEnergy == -1) {
            this.defaultSpeed = actor.stats.speed;
            this.defaultEnergy = actor.stats.energy;

            actor.stats.speed = Math.floor(actor.stats.speed / 3);
            actor.stats.energy = Math.floor(actor.stats.energy / 3);

        }
        actor.effects[this.desc].duration--;
    }
}

class StuningEffect extends Effect {
    constructor(stats) {
        super();
        this.duration = 2;
        this.desc = "Stuning";
    }

    affect(actor) {
        if (actor.effects[this.desc].duration <= 0) {
            return;
        }
        //console.log("Affecting " + actor.name + " with " + this.desc);
        //Инициализируем только один раз
        actor.stats.energy = -actor.stats.spd;
        actor.effects[this.desc].duration--;
    }
}

class InvisibilityEffect extends Effect {
    constructor(stats) {
        super();
        this.duration = 10;
        this.desc = "Invisibility";
    }

    affect(actor) {
        if (actor.effects[this.desc].duration <= 0) {
            actor.visibility = true;
            actor.sprite.alpha = 1;
            return;
        }
        //console.log("Affecting " + actor.name + " with " + this.desc);
        actor.visibility = false;
        actor.sprite.alpha = 0.5;
        this.duration--;
    }
}

class HealingEffect extends Effect {
    constructor(stats) {
        super();
        this.duration = 3;
        this.amount = stats.maxHp / 20 + Math.floor(stats.int / 2);
        this.desc = "Healing";
    }

    affect(actor) {
        if (actor.effects[this.desc].duration <= 0) {
            return;
        }
        //console.log("Affecting " + actor.name + " with " + this.desc);
        actor.stats.hp += this.amount;
        if (actor.stats.hp > actor.stats.maxHp) {
            actor.stats.hp = actor.stats.maxHp;
        }
        this.duration--;
    }
}

class PoisoningEffect extends Effect {
    constructor(stats) {
        super();
        this.duration = 10;
        this.amount = Math.floor(stats.int / 10) + 2;
        this.desc = "Poisoning";
    }

    affect(actor) {
        if (actor.effects[this.desc].duration <= 0) {
            return;
        }
        //console.log("Affecting " + actor.name + " with " + this.desc);
        actor.hp -= amount;
    }
}