class Hero extends Actor {
    constructor(position, name, spriteName, st) {
        super();
        this.x = position.x;
        this.y = position.y;
        this.dx;
        this.dy;
        this.name = name;
        this.spriteName = spriteName;
        this.sprite;
        this.nextAction = null;
        this.stats = st;
        this.race = "Human";
        this.effects = [];
        //console.log("Hero");
        //console.log(this.stats);
    }


    gainXP(enemyStats) {
        var experience = enemyStats.statsSum;
        // for (var i = 1; i < 40; i++) {
        //     this.stats.lvl++;
        //     console.log(this.stats.lvlCost());
        // }

        this.stats.exp += experience
        if (this.stats.exp > this.stats.lvlCost()) {
            this.stats.exp -= this.stats.lvlCost();
            this.stats.lvl++;
            this.stats.str++;
            this.stats.int++;
            this.stats.hp = this.stats.maxHp;
            this.stats.mp = this.stats.maxMp;

        }
        //console.log("Exp is " + this.stats.exp + " lvl is: " + this.stats.lvl);
        //console.log("Exp needed to next lvl " + this.stats.lvlCost(this.stats.lvl + 1));
    }

    takeAttack(attack) {
        //Придумать другое название переменной
        var reducedAttack = attack.getReducedAttack(this.stats);
        this.stats.hp -= reducedAttack.dmg;
        this.addEffects(reducedAttack.effs);
        // this.flash(0xff3333);
    }

    addEffects(effs) {
        if (effs == null) return;

        if (this.effects[effs.desc] === undefined) {
            this.effects[effs.desc] = effs;
            return;
        }
        this.effects[effs.desc].duration += effs.duration;
    }
}