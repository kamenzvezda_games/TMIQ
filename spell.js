class Spell {

    /**
     * Creates an instance of Spell.
     * @param type - "Projectile", "InstantCast", "SelfCast"
     * @memberOf Spell
     */
    constructor() {
        this.effect;
        this.energyCost;
        this.manaCost;
        this.attack;
        this.type;
    }

    cast() {
        //console.log("Casting smth");
    }
}

class AttackSpell extends Spell {
    constructor(stats) {
        super();
        this.effect;
        this.energyCost;
        this.manaCost;
        this.attack;
        this.rng;
        this.type;
    }

    draw(src, dir) {}

    cast(caster, x, y) {
        var actorToCastOn = game.getActor(x, y);
        //console.log("Casting on " + actorToCastOn);

        if (actorToCastOn != undefined) {
            actorToCastOn.takeAttack(this.attack);
            actorToCastOn.applyEffects();
        }
    }
}

class FireBall extends AttackSpell {
    constructor(stats) {
        super();
        this.effect = new BurningEffect(stats);
        this.energyCost = 25;
        this.manaCost = 20;
        this.attack = new RangedAttack(stats.int + 5, this.effect, 'FireBall');
        this.rng = this.attack.rng;
        this.type = "Projectile";
    }

    draw(src, dir) {
        //console.log("Fireball cost = " + this.manaCost);
        this.attack.draw(src, dir);
    }
}

class Push extends Spell {
    constructor(stats) {
        super();
        this.effect;
        this.energyCost = 15;
        this.manaCost = 20;
        this.rng = 5;
        this.type = "Projectile";
    }

    cast(caster, x, y) {
        var actorToCastOn = game.getActor(x, y);
        var dirX = Math.sign(x - caster.x);
        var dirY = Math.sign(y - caster.y);

        if (actorToCastOn != undefined) {
            //Если происходит попытка передвижения по-диагонали
            //то пусть двигается только по Y 0)0))
            if (dirX + dirY == 1 || dirX + dirY == 0) {
                dirX = 0;
            }
            //console.log("Push to " + dirX + " " + dirY);
            actorToCastOn.nextAction = new WalkAction(dirX, dirY);
        }
    }

    draw(src, dir) {
        //console.log("Push cost = " + this.manaCost);
        Attack.draw(src, dir, "Push");
    }
}

class PoisonSphere extends AttackSpell {
    constructor(stats) {
        super();
        this.effect = new BurningEffect(stats);
        this.energyCost = 5;
        this.manaCost = 10;
        this.attack = new RangedAttack(stats.int * 2 + 20, this.effect, 'PoisonSphere');
        this.rng = this.attack.rng;
        this.type = "Projectile";
    }

    draw(src, dir) {
        //console.log("PoisonSphere cost = " + this.manaCost);
        this.attack.draw(src, dir);
    }
}

class FrostBolt extends AttackSpell {
    constructor(stats) {
        super();
        this.effect = new SlowingEffect(stats);
        this.energyCost = 4;
        this.manaCost = 10;
        this.attack = new RangedAttack(stats.int * 2 + 10, this.effect, 'FrostBolt');
        this.rng = this.attack.rng;
        this.type = "Projectile";
    }

    draw(src, dir) {
        this.attack.draw(src, dir);
    }
}

class LightningBolt extends AttackSpell {
    constructor(stats) {
        super();
        this.effect = new StuningEffect(stats);
        this.energyCost = 4;
        this.manaCost = 10;
        this.attack = new RangedAttack(stats.int * 2 + 10, this.effect, 'LightningBolt');
        this.rng = this.attack.rng;
        this.type = "Projectile";
    }

    draw(src, dir) {
        this.attack.draw(src, dir);
    }
}

class Vampirism extends Spell {
    constructor(stats) {
        super();
        this.energyCost = 10;
        this.manaCost = 15 - Math.floor(stats.int / 10);
        this.attack = new RangedAttack(stats.int * 2 + 10, new BleedEffect(stats), "Vampirism");
        this.rng = 7;
        this.type = "OnPoint";
    }

    draw(src, dir) {}

    cast(caster, x, y) {
        var actorToCastOn = game.getActor(x, y);

        if (actorToCastOn != undefined) {
            caster.addEffects(new HealingEffect(caster.stats));
            caster.applyEffects();

            actorToCastOn.takeAttack(this.attack);
        }
    }
}

class Invisibility extends Spell {
    constructor(stats) {
        super();
        this.effect = new InvisibilityEffect(stats);
        this.energyCost = 10;
        this.manaCost = 5 - Math.floor(stats.int / 10);
        this.type = "InstantCast";
    }

    draw(src, dir) {}

    cast(caster, x, y) {
        var actorToCastOn = game.getActor(x, y);
        if (actorToCastOn == undefined) return;

        actorToCastOn.addEffects(this.effect);
        actorToCastOn.applyEffects();
    }
}

//Меняет здоровье на ману
class ForceExchange extends Spell {
    constructor(stats) {
        super();
        this.energyCost = 10;
        this.manaCost = 0;
        this.type = "InstantCast";
    }

    draw(src, dir) {}

    cast(caster, x, y) {
        caster.stats.hp -= Math.floor(caster.stats.maxHp / 3);
        caster.stats.mp = caster.stats.maxMp;
    }
}

class Teleportation extends Spell {
    constructor(stats) {
        super();
        this.energyCost = 20;
        this.manaCost = 15 - Math.floor(stats.int / 10);
        this.rng = 10;
        this.type = "OnPoint";
    }

    draw(src, dir) {}

    cast(caster, x, y) {
        if (caster.canOccupy(x, y)) {
            caster.x = x;
            caster.y = y;
            caster.sprite.x = x * SPRITE_SIZE;
            caster.sprite.y = y * SPRITE_SIZE;
        }
    }
}

class Heal extends Spell {
    constructor(stats) {
        super();
        this.effect = new HealingEffect(stats);
        this.energyCost = 15;
        this.manaCost = 10;
        this.type = "InstantCast";
    }

    draw(src, dir) {}

    cast(caster, x, y) {
        var actorToCastOn = game.getActor(x, y);
        if (actorToCastOn == undefined) return;

        actorToCastOn.addEffects(this.effect);
        actorToCastOn.applyEffects();
    }
}