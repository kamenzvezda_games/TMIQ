class DungeonMap extends Map{
    constructor() {
        super();
        this.Leafs = [];
        this.objects = [];
        this.availablePositions = [];
        this.previousMaps = [];
        this.warpPos;
    }

    generateMap(width, height, isBossRoom) {
        //максимальный размер комнат
        //(точнее комнат прямоугольничков, в которыхх эти комнат создаются)
        var maxLeafSize = width >= height ? width / 2 : height / 2;
        this.prepareMaps(width, height, maxLeafSize);
    }

    prepareMaps(width, height, maxLeafSize) {
        var wallMap = [];
        for (var y = 0; y < height; y++) {
            this.objects[y] = [];
            for (var x = 0; x < width; x++) {
                this.objects[y][x] = "Void";
            }
        }
        this.generateDungeon(width, height, maxLeafSize);

        this.convertToSymbolMap(width, height, this.objects);
        
        for(var i = 0; i < this.objects.length; i++)
            wallMap[i] = this.objects[i].slice();

        this.placeWalls(wallMap, width, height);

        this.getAvailablePositions(width, height, wallMap, ["Masonry"]);

        this.convertToTileMap(this.objects, width, height, true, true, this.previousMaps, "Void");
        this.convertToTileMap(wallMap, width, height, true, false, this.previousMaps, "Masonry");

        this.copyObjectsToMap(width, height, wallMap, this.objects, "Masonry", false);
        this.placeWarpPoint(width);
    }

    placeWalls(map, width, height) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                if (map[y][x] === "Masonry") {
                            if(this.checkOnSecondaryTile(map, y - 1, x - 1, "Void") || this.checkOnSecondaryTile(map, y - 1, x, "Void")
                            || this.checkOnSecondaryTile(map, y - 1, x + 1, "Void") || this.checkOnSecondaryTile(map, y, x - 1, "Void")
                            || this.checkOnSecondaryTile(map, y, x + 1, "Void") || this.checkOnSecondaryTile(map, y + 1, x - 1, "Void") 
                            || this.checkOnSecondaryTile(map, y + 1, x, "Void") || this.checkOnSecondaryTile(map, y + 1, x + 1, "Void")) {
                                map[y][x] = "MasonryWall";
                    }
                }
            }
        }
    }

    generateDungeon(width, height, maxLeafSize) {
        var root = new Leaf(0, 0, width, height);
        this.Leafs.push(root);
        var didSplit = true;
        while (didSplit) {
            didSplit = false;
            for (var i = 0; i < this.Leafs.length; i++)
                if (this.Leafs[i].leftChild === undefined && this.Leafs[i].rightChild === undefined)
                    if (this.Leafs[i].width > maxLeafSize || this.Leafs[i].height > maxLeafSize || Math.random() <= 0.75)
                        if (this.Leafs[i].split()) {
                            this.Leafs.push(this.Leafs[i].leftChild);
                            this.Leafs.push(this.Leafs[i].rightChild);
                            didSplit = true;
                        }
        }
        root.createRooms();
    }

    convertToSymbolMap(width, height, map) {
        for (var i = 0; i < this.Leafs.length; i++) {
            for (var j = 0; j < this.Leafs[i].halls.length; j++) {
                this.fillRectangle(map, this.Leafs[i].halls[j], "Masonry");
            }
            this.fillRectangle(map, this.Leafs[i].room, "Masonry");
        }
    }

    placeWarpPoint(width) {
        var pos = this.getRandomAvailablePos(width, this.availablePositions);
        while(!this.checkSurroundedTiles(this.objects, pos.y, pos.x, "Void", "Masonry"))
            pos = this.getRandomAvailablePos(width, this.availablePositions);
        var spriteName = "Warp1";
        if (isBossRoom)
            spriteName = "Masonry1";
        this.objects[pos.y][pos.x].sprite = phaserGame.add.sprite(pos.x * SPRITE_SIZE, pos.y * SPRITE_SIZE, spriteName);
        this.warpPos = {
            x: pos.x,
            y: pos.y
        };
    }

    showWarp() {
        var pos = this.warpPos;
        this.objects[pos.y][pos.x].sprite.loadTexture("Warp1");
    }

    getAvailablePositions(width, height, map, passableSymbols) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                if (passableSymbols.includes(map[y][x]))
                    this.availablePositions.push({
                        x: x,
                        y: y
                    });
            }
        }
    }
}

class Rectangle {
    constructor(x, y, width, height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}

//делим карту на прямогульники (листья) методом двоичного ращбиения простарнства
class Leaf {
    constructor(x, y, width, height) {
        this.minLeafSize = 15;
        this.minRoomSize = 14;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.leftChild;
        this.rightChild;
        this.room = 0;
        this.halls = [];
    }

    split() {
        if (this.leftChild != undefined || this.rightChild != undefined)
            return false;
        var splitH = Math.random() > 0.5;
        if (this.width > this.height && this.width / this.height >= 1.25)
            splitH = false;
        else if (this.width < this.height && this.height / this.width >= 1.25)
            splitH = true;
        var max = (splitH ? this.height : this.width) - this.minLeafSize;
        if (max <= this.minLeafSize)
            return false;
        var split = Random(this.minLeafSize, max);
        if (splitH) {
            this.leftChild = new Leaf(this.x, this.y, this.width, split);
            this.rightChild = new Leaf(this.x, this.y + split, this.width, this.height - split);
        } else {
            this.leftChild = new Leaf(this.x, this.y, split, this.height);
            this.rightChild = new Leaf(this.x + split, this.y, this.width - split, this.height);
        }
        return true;
    }

    createRooms() {
        if (this.leftChild != undefined || this.rightChild != undefined) {
            if (this.leftChild != undefined)
                this.leftChild.createRooms();
            if (this.rightChild != undefined)
                this.rightChild.createRooms();
            if (this.leftChild != undefined && this.rightChild != undefined)
                this.createHall(this.leftChild.getRoom(), this.rightChild.getRoom());
        } else {
            var roomWidth = Random(this.minRoomSize, this.width - 2);
            var roomHeight = Random(this.minRoomSize, this.height - 2);
            var roomX = Random(1, this.width - roomWidth - 1);
            var roomY = Random(1, this.height - roomHeight - 1);
            this.room = new Rectangle(roomX + this.x, roomY + this.y, roomWidth, roomHeight);
        }
    }

    getRoom() {
        if (this.room != 0)
            return this.room;
        var lRoom = this.leftChild.getRoom();
        var rRoom = this.rightChild.getRoom();
        if (lRoom === undefined && rRoom === undefined)
            return undefined;
        if (rRoom === undefined)
            return lRoom;
        if (lRoom === undefined)
            return rRoom;
        return (Math.random > 0.5 ? lRoom : rRoom);
    }

    createHall(l, r) {
        var point1 = {
            x: Random(l.x + 3, l.x + l.width - 4),
            y: Random(l.y + 3, l.y + l.height - 4)
        };
        var point2 = {
            x: Random(r.x + 3, r.x + r.width - 4),
            y: Random(r.y + 3, r.y + r.height - 4)
        };
        var w = point2.x - point1.x;
        var h = point2.y - point1.y;
        //ой все
        if (w < 0) {
            if (h < 0) {
                if (Math.random() < 0.5) {
                    this.halls.push(new Rectangle(point2.x, point1.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point2.x, point2.y, 3, Math.abs(h) + 3));
                } else {
                    this.halls.push(new Rectangle(point2.x, point2.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point1.x, point2.y, 3, Math.abs(h) + 3));
                }
            } else if (h > 0) {
                if (Math.random() < 0.5) {
                    this.halls.push(new Rectangle(point2.x, point1.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point2.x, point1.y, 3, Math.abs(h) + 3));
                } else {
                    this.halls.push(new Rectangle(point2.x, point2.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point1.x, point1.y, 3, Math.abs(h) + 3));
                }
            } else
                this.halls.push(new Rectangle(point2.x, point2.y, Math.abs(w) + 3, 3));
        } else if (w > 0) {
            if (h < 0) {
                if (Math.random() < 0.5) {
                    this.halls.push(new Rectangle(point1.x, point2.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point1.x, point2.y, 3, Math.abs(h) + 3));
                } else {
                    this.halls.push(new Rectangle(point1.x, point1.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point2.x, point2.y, 3, Math.abs(h) + 3));
                }
            } else if (h > 0) {
                if (Math.random() < 0.5) {
                    this.halls.push(new Rectangle(point1.x, point1.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point2.x, point1.y, 3, Math.abs(h) + 3));
                } else {
                    this.halls.push(new Rectangle(point1.x, point2.y, Math.abs(w) + 3, 3));
                    this.halls.push(new Rectangle(point1.x, point1.y, 3, Math.abs(h) + 3));
                }
            } else
                this.halls.push(new Rectangle(point1.x, point1.y, Math.abs(w) + 3, 3));
        } else {
            if (h < 0)
                this.halls.push(new Rectangle(point2.x, point2.y, 3, Math.abs(h) + 3));
            else if (h > 0)
                this.halls.push(new Rectangle(point1.x, point1.y, 3, Math.abs(h) + 3));
        }
    }
}