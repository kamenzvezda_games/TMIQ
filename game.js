"use strict"

const SPRITE_SIZE = 48;
const COLS = 40 * 2;
const ROWS = 20 * 2;
const PHASER_WIN_WIDTH = 20 * SPRITE_SIZE;
const PHASER_WIN_HEIGHT = 13 * SPRITE_SIZE;

const HERO = 0;

var phaserGame = new Phaser.Game(PHASER_WIN_WIDTH, PHASER_WIN_HEIGHT, Phaser.AUTO, null);

class Game {
    constructor(cols, rows, spriteSize) {
        this.actors = [];
        this.map = new ForestMap();
        this.statistics;
        this.currentActorIndex = 0;
        this.relations = this.initRelations();
    }

    get hero() {
        return this.actors[HERO];
    }

    nextMap() {
        isBossRoom = ((stage + 1) % 5 === 0) ? true : false;
        phaserGame.state.start("GameState");
    }

    getDistance(source, dest) {
        return Math.sqrt(Math.pow(source.x - dest.x, 2) + Math.pow(source.y - dest.y, 2));
    }

    initRelations() {
        return [
            { race: "Human", Monster: true, Human: false, Orc: true, Goblin: true },
            { race: "Monster", Human: true, Monster: false, Orc: true, Goblin: false },
            { race: "Orc", Human: true, Monster: true, Orc: false, Goblin: false },
            { race: "Goblin", Human: true, Monster: false, Orc: false, Goblin: false }
        ];
    }

    currentActor() {
        return this.actors[this.currentActorIndex];
    }

    getNextActor() {
        this.currentActorIndex = (this.currentActorIndex + 1) % this.actors.length;
        return this.actors[this.currentActorIndex];
    }

    updateMobs() {
        //Цикл с первого актера, потому что нулевой - Главный 
        for (var i = 1; i < this.actors.length; i++) {
            //Назначаем новое действие только если старое уже выполнено
            if (this.actors[i].nextAction === null) {
                this.actors[i].nextAction = this.actors[i].behavior.behave(this.actors[i]);
                this.actors[i].updateBar();
            }
        }
    }

    getActor(x, y) {
        return this.actors.find((actor) => {
            return actor.x == x && actor.y == y
        });
    }

    addActor(actor) {
        game.actors.push(actor);
        let actorsLen = game.actors.length - 1;
        game.actors[actorsLen].sprite = phaserGame.add.sprite(game.actors[actorsLen].x * SPRITE_SIZE, game.actors[actorsLen].y * SPRITE_SIZE, game.actors[actorsLen].spriteName);
        game.actors[actorsLen].sprite.bringToTop();

    }

    isInBorders(x, y) {
        return (x < COLS && x >= 0 && y < ROWS && y >= 0);
    }
}

var PreloadState = {
    preload: function() {
        phaserGame.load.image('Logo', 'assets/Misc/Logo.png');
    },

    create: function() {
        phaserGame.state.start("LoadState");
    }
};

var LoadState = {
    preload: function() {

        phaserGame.load.bitmapFont('Font', 'assets/Misc/font.png', 'assets/Misc/font.fnt');

        var logo = phaserGame.add.sprite(0, 0, 'Logo');
        phaserGame.load.image('GUI', 'assets/Misc/GUI.png');
        phaserGame.load.image('Frame', 'assets/Misc/Frame.png');
        phaserGame.load.image('MeleeAttack', 'assets/Misc/MeleeAttack.png');
        phaserGame.load.image('Rock', 'assets/Misc/Rock.png');
        phaserGame.load.image('Wheel', 'assets/Misc/Wheel.png');
        phaserGame.load.image('Void1', 'assets/Misc/Void1.png');
        phaserGame.load.image('Warp1', 'assets/Misc/Warp1.png');
        phaserGame.load.image('GameOverScreen', 'assets/Misc/GameOverScreen.png');
        phaserGame.load.image('MenuScreen', 'assets/Misc/MenuScreen.png');
        phaserGame.load.image('Help', 'assets/Misc/Help.png');
        phaserGame.load.spritesheet('TryAgainButton', 'assets/Misc/TryAgainButton.png', 294, 87, 2);
        phaserGame.load.spritesheet('MenuButton', 'assets/Misc/MenuButton.png', 294, 87, 2);

        phaserGame.load.image('FireBall', 'assets/Spells/FireBall.png');
        phaserGame.load.image('LightningBolt', 'assets/Spells/LightningBolt.png');
        phaserGame.load.image('PoisonSphere', 'assets/Spells/PoisonSphere.png');
        phaserGame.load.image('FrostBolt', 'assets/Spells/FrostBolt.png');
        phaserGame.load.image('Push', 'assets/Spells/Push.png');


        phaserGame.load.image('Hero', 'assets/Mobs/Hero.png');
        phaserGame.load.image('SlimeBlue1', 'assets/Mobs/SlimeBlue1.png');
        phaserGame.load.image('SlimeRed1', 'assets/Mobs/SlimeRed1.png');
        phaserGame.load.image('SlimeYellow1', 'assets/Mobs/SlimeYellow1.png');
        phaserGame.load.image('Orc1', 'assets/Mobs/Orc1.png');
        phaserGame.load.image('Orc2', 'assets/Mobs/Orc2.png');
        phaserGame.load.image('Orc3', 'assets/Mobs/Orc3.png');
        phaserGame.load.image('Orc4', 'assets/Mobs/Orc4.png');
        phaserGame.load.image('Bat1', 'assets/Mobs/Bat1.png');
        phaserGame.load.image('Dwarf1', 'assets/Mobs/Dwarf1.png');
        phaserGame.load.image('OrcMage1', 'assets/Mobs/OrcMage1.png');
        phaserGame.load.image('Goblin1', 'assets/Mobs/Goblin1.png');
        phaserGame.load.image('Ghost1', 'assets/Mobs/Ghost1.png');
        phaserGame.load.image('Mage1', 'assets/Mobs/Mage1.png');
        phaserGame.load.image('Skeleton1', 'assets/Mobs/Skeleton1.png');
        phaserGame.load.image('Zombie1', 'assets/Mobs/Zombie1.png');
        phaserGame.load.image('Zombie2', 'assets/Mobs/Zombie2.png');
        phaserGame.load.image('TheWheelMan0', 'assets/Mobs/GachiBoss1.png');
        phaserGame.load.image('WheelManBrother1', 'assets/Mobs/GachiFreak1.png');
        phaserGame.load.image('WheelManBrother2', 'assets/Mobs/GachiFreak2.png');
        phaserGame.load.image('WheelManBrother3', 'assets/Mobs/GachiFreak3.png');


        phaserGame.load.image('Grass1', 'assets/Tiles/Grass/Grass1.png');
        phaserGame.load.image('Grass2', 'assets/Tiles/Grass/Grass2.png');
        phaserGame.load.image('Grass3', 'assets/Tiles/Grass/Grass3.png');
        phaserGame.load.image('Grass4', 'assets/Tiles/Grass/Grass4.png');
        phaserGame.load.image('Grass5', 'assets/Tiles/Grass/Grass5.png');
        phaserGame.load.image('Grass6', 'assets/Tiles/Grass/Grass6.png');
        phaserGame.load.image('Grass7', 'assets/Tiles/Grass/Grass7.png');
        phaserGame.load.image('Grass8', 'assets/Tiles/Grass/Grass8.png');

        phaserGame.load.image('Bush1', 'assets/Tiles/Bush/Bush1.png');
        phaserGame.load.image('Bush2', 'assets/Tiles/Bush/Bush2.png');
        phaserGame.load.image('BushDown1', 'assets/Tiles/Bush/BushDown1.png');
        phaserGame.load.image('BushDown2', 'assets/Tiles/Bush/BushDown2.png');
        phaserGame.load.image('BushLeft1', 'assets/Tiles/Bush/BushLeft1.png');
        phaserGame.load.image('BushLeft2', 'assets/Tiles/Bush/BushLeft2.png');
        phaserGame.load.image('BushUp1', 'assets/Tiles/Bush/BushUp1.png');
        phaserGame.load.image('BushUp2', 'assets/Tiles/Bush/BushUp2.png');
        phaserGame.load.image('BushRight1', 'assets/Tiles/Bush/BushRight1.png');
        phaserGame.load.image('BushRight2', 'assets/Tiles/Bush/BushRight2.png');
        phaserGame.load.image('BushLeftDown1', 'assets/Tiles/Bush/BushLeftDown1.png');
        phaserGame.load.image('BushLeftUp1', 'assets/Tiles/Bush/BushLeftUp1.png');
        phaserGame.load.image('BushRightDown1', 'assets/Tiles/Bush/BushRightDown1.png');
        phaserGame.load.image('BushRightUp1', 'assets/Tiles/Bush/BushRightUp1.png');
        phaserGame.load.image('BushLeftDownUp1', 'assets/Tiles/Bush/BushLeftDownUp1.png');
        phaserGame.load.image('BushRightDownUp1', 'assets/Tiles/Bush/BushRightDownUp1.png');
        phaserGame.load.image('BushRightLeftDown1', 'assets/Tiles/Bush/BushRightLeftDown1.png');
        phaserGame.load.image('BushRightLeftUp1', 'assets/Tiles/Bush/BushRightLeftUp1.png');

        phaserGame.load.image('Ground1', 'assets/Tiles/Ground/Ground1.png');
        phaserGame.load.image('Ground2', 'assets/Tiles/Ground/Ground2.png');
        phaserGame.load.image('Ground3', 'assets/Tiles/Ground/Ground3.png');
        phaserGame.load.image('Ground4', 'assets/Tiles/Ground/Ground4.png');
        phaserGame.load.image('Ground5', 'assets/Tiles/Ground/Ground5.png');
        phaserGame.load.image('Ground6', 'assets/Tiles/Ground/Ground6.png');
        phaserGame.load.image('Ground7', 'assets/Tiles/Ground/Ground7.png');
        phaserGame.load.image('Ground8', 'assets/Tiles/Ground/Ground8.png');
        phaserGame.load.image('GroundGrassDown1', 'assets/Tiles/Ground/GroundGrassDown1.png');
        phaserGame.load.image('GroundGrassDown2', 'assets/Tiles/Ground/GroundGrassDown2.png');
        phaserGame.load.image('GroundGrassLeft1', 'assets/Tiles/Ground/GroundGrassLeft1.png');
        phaserGame.load.image('GroundGrassLeft2', 'assets/Tiles/Ground/GroundGrassLeft2.png');
        phaserGame.load.image('GroundGrassUp1', 'assets/Tiles/Ground/GroundGrassUp1.png');
        phaserGame.load.image('GroundGrassUp2', 'assets/Tiles/Ground/GroundGrassUp2.png');
        phaserGame.load.image('GroundGrassRight1', 'assets/Tiles/Ground/GroundGrassRight1.png');
        phaserGame.load.image('GroundGrassRight2', 'assets/Tiles/Ground/GroundGrassRight2.png');
        phaserGame.load.image('GroundGrassLeftDown1', 'assets/Tiles/Ground/GroundGrassLeftDown1.png');
        phaserGame.load.image('GroundGrassLeftUp1', 'assets/Tiles/Ground/GroundGrassLeftUp1.png');
        phaserGame.load.image('GroundGrassRightDown1', 'assets/Tiles/Ground/GroundGrassRightDown1.png');
        phaserGame.load.image('GroundGrassRightUp1', 'assets/Tiles/Ground/GroundGrassRightUp1.png');
        phaserGame.load.image('GrassGroundRightUp1', 'assets/Tiles/Grass/GrassGroundRightUp1.png');
        phaserGame.load.image('GrassGroundRightDown1', 'assets/Tiles/Grass/GrassGroundRightDown1.png');
        phaserGame.load.image('GrassGroundLeftUp1', 'assets/Tiles/Grass/GrassGroundLeftUp1.png');
        phaserGame.load.image('GrassGroundLeftDown1', 'assets/Tiles/Grass/GrassGroundLeftDown1.png');
        phaserGame.load.image('GroundGrassLeftDownUp1', 'assets/Tiles/Ground/GroundGrassLeftDownUp1.png');
        phaserGame.load.image('GroundGrassRightDownUp1', 'assets/Tiles/Ground/GroundGrassRightDownUp1.png');
        phaserGame.load.image('GroundGrassRightLeftUp1', 'assets/Tiles/Ground/GroundGrassRightLeftUp1.png');
        phaserGame.load.image('GroundGrassRightLeftDown1', 'assets/Tiles/Ground/GroundGrassRightLeftDown1.png');

        phaserGame.load.image('GrassGroundRightDownLeftUp1', 'assets/Tiles/Grass/GrassGroundRightDownLeftUp1.png');
        phaserGame.load.image('GrassGroundRightUpLeftDown1', 'assets/Tiles/Grass/GrassGroundRightUpLeftDown1.png');
        phaserGame.load.image('GrassGroundRightDownLeftDown1', 'assets/Tiles/Grass/GrassGroundRightDownLeftDown1.png');
        phaserGame.load.image('GrassGroundLeftDownLeftUp1', 'assets/Tiles/Grass/GrassGroundLeftDownLeftUp1.png');
        phaserGame.load.image('GrassGroundRightDownRightUp1', 'assets/Tiles/Grass/GrassGroundRightDownRightUp1.png');
        phaserGame.load.image('GrassGroundRightUpLeftUp1', 'assets/Tiles/Grass/GrassGroundRightUpLeftUp1.png');

        phaserGame.load.image('Stone1', 'assets/Tiles/Stone/Stone1.png');
        phaserGame.load.image('Stone2', 'assets/Tiles/Stone/Stone2.png');
        phaserGame.load.image('Stone3', 'assets/Tiles/Stone/Stone3.png');
        phaserGame.load.image('Stone4', 'assets/Tiles/Stone/Stone4.png');
        phaserGame.load.image('StoneLeft1', 'assets/Tiles/Stone/StoneLeft1.png');
        phaserGame.load.image('StoneLeft2', 'assets/Tiles/Stone/StoneLeft2.png');
        phaserGame.load.image('StoneLeft3', 'assets/Tiles/Stone/StoneLeft3.png');
        phaserGame.load.image('StoneRight1', 'assets/Tiles/Stone/StoneRight1.png');
        phaserGame.load.image('StoneRight2', 'assets/Tiles/Stone/StoneRight2.png');
        phaserGame.load.image('StoneRight3', 'assets/Tiles/Stone/StoneRight3.png');
        phaserGame.load.image('StoneDown1', 'assets/Tiles/Stone/StoneDown1.png');
        phaserGame.load.image('StoneDown2', 'assets/Tiles/Stone/StoneDown2.png');
        phaserGame.load.image('StoneUp1', 'assets/Tiles/Stone/StoneUp1.png');
        phaserGame.load.image('StoneUp2', 'assets/Tiles/Stone/StoneUp2.png');
        phaserGame.load.image('StoneRightLeftUp1', 'assets/Tiles/Stone/StoneRightLeftUp1.png');
        phaserGame.load.image('StoneRightLeftDown1', 'assets/Tiles/Stone/StoneRightLeftDown1.png');
        phaserGame.load.image('StoneRightDownUp1', 'assets/Tiles/Stone/StoneRightDownUp1.png');
        phaserGame.load.image('StoneLeftDownUp1', 'assets/Tiles/Stone/StoneLeftDownUp1.png');
        phaserGame.load.image('StoneRightDown1', 'assets/Tiles/Stone/StoneRightDown1.png');
        phaserGame.load.image('StoneRightUp1', 'assets/Tiles/Stone/StoneRightUp1.png');
        phaserGame.load.image('StoneLeftUp1', 'assets/Tiles/Stone/StoneLeftUp1.png');
        phaserGame.load.image('StoneLeftDown1', 'assets/Tiles/Stone/StoneLeftDown1.png');

        phaserGame.load.image('Masonry1', 'assets/Tiles/Masonry/Masonry1.png');
        phaserGame.load.image('Masonry2', 'assets/Tiles/Masonry/Masonry2.png');
        phaserGame.load.image('Masonry3', 'assets/Tiles/Masonry/Masonry3.png');
        phaserGame.load.image('Masonry4', 'assets/Tiles/Masonry/Masonry4.png');
        phaserGame.load.image('Masonry5', 'assets/Tiles/Masonry/Masonry5.png');
        phaserGame.load.image('MasonryWall1', 'assets/Tiles/Masonry/MasonryWall1.png');
    },

    create: function() {
        phaserGame.state.start("MenuState");
    }

};

var MenuState = {
    create: function() {
        var logo = phaserGame.add.sprite(0, 0, 'MenuScreen');
        var newGameButton = phaserGame.add.button(335, 280, 'MenuButton', actionOnClick, this, 0, 1, 2);
        var newGameText = phaserGame.add.bitmapText(355, 310, 'Font', 'New adventure', 35);
        var helpButton = phaserGame.add.button(335, 420, 'MenuButton', helpClick, this, 0, 1, 2);
        var helpText = phaserGame.add.bitmapText(443, 450, 'Font', 'Help', 35);
    }
};

var HelpState = {
    create: function() {
        var help = phaserGame.add.sprite(0, 0, 'Help');
    },

    update: function() {
        phaserGame.input.keyboard.addKey(Phaser.Keyboard.ESC).onDown.add(pauseFunction, this);
    }
};

var pauseFunction = () => {
    phaserGame.state.start("MenuState")
};

var stage = 0;
var text;
var isBossRoom = false;
var statistics = new Statistics();
var stageText;
var gui;
var hero = new Hero({
    x: 0,
    y: 0
}, "HeroName", "Hero", new Stats({
    vit: 10,
    str: 5,
    agl: 5,
    spd: 5,
    lvl: 1
}));

var GameState = {
    create: function() {
        stage++;

        game = new Game(COLS, ROWS, SPRITE_SIZE);
        game.statistics = statistics;

        phaserGame.physics.startSystem(Phaser.Physics.ARCADE);
        // init keyboard commands
        phaserGame.input.keyboard.addCallbacks(null, null, onKeyDown);
        phaserGame.input.keyboard.enabled = true;
        phaserGame.world.setBounds(0, 0, COLS * SPRITE_SIZE, ROWS * SPRITE_SIZE);
        if (isBossRoom) {
            game.map = new DungeonMap();
            game.map.generateMap(20, 20);
        } else {
            game.map = Math.random() >= 0.5 ? new ForestMap() : new DungeonMap();
            game.map.generateMap(COLS, ROWS);
        }
        var pos = game.map.getRandomAvailablePos(COLS, game.map.availablePositions);
        var posWidth = game.map.constructor.name === "ForestMap" ? 3 : COLS;
        var pos = game.map.getRandomAvailablePos(posWidth, game.map.availablePositions);
        hero.x = pos.x;
        hero.y = pos.y;
        game.addActor(hero);

        var mobsNum = isBossRoom ? 4 : Random(14, 22);
        spawnRandomMobs(mobsNum);


        phaserGame.camera.follow(game.hero.sprite, Phaser.Camera.FOLLOW_LOCKON, 0.3, 0.3);
        //Центрирование камеры (уменьшение высоты для того что бы камера корректно работала
        //с интерфейсом
        phaserGame.camera.setSize(PHASER_WIN_WIDTH, PHASER_WIN_HEIGHT - SPRITE_SIZE * 1.5);

        initBars();

        gui = phaserGame.add.sprite(0, 48, 'GUI');
        gui.fixedToCamera = true;
        gui.bringToTop();

        text = phaserGame.add.bitmapText(12, 558, 'Font', 'Level: 1', 30);
        text.fixedToCamera = true;
        stageText = phaserGame.add.bitmapText(12, 588, 'Font', 'Stage: 1', 30);
        stageText.fixedToCamera = true;


    },

    update: function() {
        if (isBossRoom && game.actors.length === 1)
            game.map.showWarp();
        if (game.map.checkOnWarpPosition(game.hero.y, game.hero.x, game.map) && (!isBossRoom || game.actors.length === 1)) {
            statistics = game.statistics
            hero = game.hero;
            game.nextMap();
            game.statistics.levelDiscovered();
        }

        updateInterface();
        killDeadActors();

        //////console.log(game.actors.length);
        var actor = game.currentActor();
        var action = actor.nextAction;

        while (action != null) {
            actor.applyEffects();
            //////console.log("Actor " + game.currentActorIndex + " being handling");
            if (actor.canPerformAction()) {
                actor.performAction();
                //////console.log("Actor " + game.currentActorIndex + " PERFORMED ACTION!1!");
            } else {
                //Если игроку надо отдыхать, то пока он не отдохнул нужно обрабатывать
                //остальных мобов.
                if (game.currentActorIndex == HERO) {
                    // ////console.log("Hero resting!");
                    game.updateMobs();
                    actor.rest();
                } else {
                    actor.nextAction = new RestAction();
                    actor.performAction();
                }

            }
            actor = game.getNextActor();
            action = actor.nextAction;
        }

        game.updateMobs();

        //Костыль, котороый обработает всех актеров верно, и на след. итерации обрабока начнется с ГГ
        while (game.currentActorIndex != HERO) {
            //////console.log("in 2nd while");
            //////console.log("Actor " + game.currentActorIndex + " being handling");

            if (actor.canPerformAction()) {
                actor.performAction();
                //////console.log("Actor " + game.currentActorIndex + " PERFORMED ACTION!1!");

            } else {
                actor.nextAction = new RestAction();
                actor.performAction();
                //////console.log("Actor " + game.currentActorIndex + " is resting!");

            }
            actor = game.getNextActor();
            action = actor.nextAction;
        }
    }
};

function killDeadActors() {
    if (game.hero.stats.hp <= 0) {
        //без setTimeout fade не делается, а просто вызывается GameOverState
        phaserGame.input.keyboard.enabled = false;
        phaserGame.camera.fade(0x000000, 900);
        setTimeout('phaserGame.state.start("GameOverState")', 800);
    }

    for (var i = 1; i < game.actors.length; i++) {

        //Если моб умер от рук ГГ то ГГ начисляется опыт
        if (game.actors[i].stats.hp <= 0) {
            game.actors[i].hpBar.kill();
            if (game.actors[i].attacker == game.hero.name) {
                game.hero.gainXP(game.actors[i].stats);
                game.statistics.mobKilled(game.actors[i]);
            }

            game.actors[i].sprite.kill();
            game.actors.splice(i, 1);
        }
    }
}

var GameOverState = {
    create: function() {
        myHealthBar.kill();
        isCasting = false;
        isBossRoom = false;
        if (frame != undefined) {
            frame.kill;
        }
        spellToCast = undefined;
        stage = 0;
        myExpBar.kill();
        myManaBar.kill();
        statistics = new Statistics();
        hero = new Hero({
            x: 0,
            y: 0
        }, "HeroName", "Hero", new Stats({
            vit: 10,
            str: 5,
            agl: 5,
            spd: 5,
            lvl: 1
        }));
        var GameOverScreen = phaserGame.add.sprite(0, 0, 'GameOverScreen');
        var statisticsText = phaserGame.add.bitmapText(200, 200, 'Font', game.statistics.getStatistics(), 25);
        //  We'll set the bounds to be from x0, y100 and be 800px wide by 100px high
        var button = phaserGame.add.button(335, 530, 'TryAgainButton', actionOnClick, this, 0, 1, 2);
    }
};

function getRandomSprite(spriteName) {
    var objectQuantity = 0;
    while (phaserGame.cache.checkImageKey(spriteName + (objectQuantity + 1).toString()))
        objectQuantity++;
    return (spriteName + Random(1, objectQuantity)).toString();
}

function spawnRandomMobs(num) {
    var heroLvl = game.hero.stats.lvl / 2;
    var pos = game.map.availablePositions;
    var forestMobs = [
        { name: "Mage", race: "Monster", stats: { vit: 1 + stage * 2, str: 1 + stage * 4, agl: 1, spd: 1, lvl: 1 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 1, null)], spells: [new FireBall(new Stats({ vit: 1 + stage * 4, str: 1 + stage * 3, agl: 1, spd: 1, lvl: 1 + stage * 4 }))] },
        { name: "OrcMage", race: "Monster", stats: { vit: 2 + stage * 2, str: 2 + stage * 4, agl: 1, spd: 1, lvl: 1 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 1, null)], spells: [new FireBall(new Stats({ vit: 1 + stage * 2, str: 1 + stage * 2, agl: 1, spd: 1, lvl: 1 + stage * 2 }))] },

        { name: "Orc", race: "Orc", stats: { vit: 3 + stage * 4, str: 3 + stage * 4, agl: 3, spd: 2, lvl: 3 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 10, null)] },
        { name: "Goblin", race: "Goblin", stats: { vit: 2 + stage * 4, str: 2 + stage * 4, agl: 3, spd: 4, lvl: 1 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 2, null), new StoneAttack(stage * 2 + 5, null)] },
        { name: "SlimeRed", race: "Monster", stats: { vit: 2 + stage * 4, str: 2 + stage * 4, agl: 2, spd: 2, lvl: 1 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 7, null)] },
        { name: "SlimeBlue", race: "Monster", stats: { vit: 2 + stage * 4, str: 2 + stage * 4, agl: 3, spd: 2, lvl: 1 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 7, null)] },
        { name: "SlimeYellow", race: "Monster", stats: { vit: 2 + stage * 4, str: 2 + stage * 4, agl: 2, spd: 2, lvl: 1 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 4, null)] }
    ];

    var dungeonMobs = [
        { name: "Ghost", race: "Monster", stats: { vit: 5 + stage * 4, str: 4 + stage * 4, agl: 5, spd: 25, lvl: 2 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 4, null)] },
        { name: "Zombie", race: "Monster", stats: { vit: 7 + stage * 4, str: 4 + stage * 4, agl: 3, spd: 2, lvl: 3 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 9, null)] },
        { name: "Skeleton", race: "Monster", stats: { vit: 7 + stage * 4, str: 4 + stage * 4, agl: 3, spd: 2, lvl: 3 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 9, null)] },
        { name: "Bat", race: "Monster", stats: { vit: 4 + stage * 4, str: 4 + stage * 4, agl: 5, spd: 20, lvl: 3 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 10, null)] },
        { name: "Dwarf", race: "Monster", stats: { vit: 7 + stage * 4, str: 8 + stage * 4, agl: 4, spd: 3, lvl: 3 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 10, null)] }
    ];

    var gachiMobs = [
        { name: "TheWheelMan", race: "Monster", stats: { vit: 15 + stage * 4 + heroLvl / 2, str: 12 + stage * 4 + heroLvl / 2, agl: 7, spd: 7, lvl: 4 + stage * 4 + heroLvl / 2 }, attack: [new MeleeAttack(stage * 5 + heroLvl, null), new WheelAttack(stage * 3 + 2, null)] },
        { name: "WheelManBrother", race: "Monster", stats: { vit: 10 + stage * 4, str: 10 + stage * 4, agl: 6, spd: 8, lvl: 5 + stage * 4 }, attack: [new MeleeAttack(stage * 4 + 15, null)] }
    ];
    for (var i = 0; i < forestMobs.length; i++)
        dungeonMobs.unshift(forestMobs[i]);
    var mobs = game.map.constructor.name === "ForestMap" ? forestMobs : dungeonMobs;
    if (num === 4) {
        mobs = gachiMobs;
    }
    for (var i = 0; i < num; i++) {
        var mobNum = Random(0, mobs.length - 1);
        var mob = mobs[mobNum];
        var mobPos = game.map.getRandomAvailablePos(COLS, pos);
        if (isBossRoom) {
            mobNum = (i === 0) ? 0 : 1;
            mob = mobs[mobNum];
            game.addActor(new Mob(mobPos, mob.name, mob.name + i.toString(), new MobBehavior(),
                new Stats({ vit: mob.stats.vit, str: mob.stats.str, agl: mob.stats.agl, spd: mob.stats.spd, lvl: mob.stats.lvl }),
                mob.race, mob.attack, mob.spells));
            game.actors[i + 1].initHpBar();
        } else {
            game.addActor(new Mob(mobPos, mob.name, getRandomSprite(mob.name), new MobBehavior(),
                new Stats({ vit: mob.stats.vit, str: mob.stats.str, agl: mob.stats.agl, spd: mob.stats.spd, lvl: mob.stats.lvl }),
                mob.race, mob.attack, mob.spells));
            game.actors[i + 1].initHpBar();
        }
    }
}


function updateInterface() {
    text.setText(("Level: " + game.hero.stats.lvl.toString()));
    stageText.setText(("Stage: " + stage.toString()));


    myHealthBar.setPercent(game.hero.stats.hp * 100 / game.hero.stats.maxHp);
    myExpBar.setPercent(game.hero.stats.exp * 100 / game.hero.stats.lvlCost());
    myManaBar.setPercent(game.hero.stats.mp * 100 / game.hero.stats.maxMp);
}

var myHealthBar, myManaBar, myExpBar;


function initBars() {
    //Ниже лютый хардкод координат баров. Не стоит менять координаты

    var hpBarConfig = { x: 848, y: 568, bg: { color: '#871646' }, bar: { color: '#e03c28' } };
    myHealthBar = new Bar(phaserGame, hpBarConfig);
    var mpBarConfig = {
        x: 848,
        y: 588,
        bg: {
            color: '#39709d'
        },
        bar: {
            color: '#229ad7'
        }
    };
    myManaBar = new Bar(phaserGame, mpBarConfig);
    var expBarConfig = {
        x: 848,
        y: 608,
        bg: {
            color: '#8f541c'
        },
        bar: {
            color: '#c27b23'
        }
    };
    myExpBar = new Bar(phaserGame, expBarConfig);
    myHealthBar.setPercent(game.actors[HERO].stats.hp * 100 / game.actors[HERO].stats.MaxHp);
    myExpBar.setPercent(game.actors[HERO].stats.exp * 100 / game.actors[HERO].stats.lvlCost());
    myManaBar.setPercent(game.actors[HERO].stats.mp * 100 / game.actors[HERO].stats.MaxMp);
}

var actionOnClick = () => {
    phaserGame.state.start("GameState");
}

var helpClick = () => {
    phaserGame.state.start("HelpState");
}


var game = new Game(COLS, ROWS, SPRITE_SIZE);

phaserGame.state.add("PreloadState", PreloadState);
phaserGame.state.add("LoadState", LoadState);
phaserGame.state.add("MenuState", MenuState);
phaserGame.state.add("GameState", GameState);
phaserGame.state.add("GameOverState", GameOverState);
phaserGame.state.add("HelpState", HelpState);

phaserGame.state.start("PreloadState");

var isCasting = false;
var frame;
var spellToCast;

function moveFrame(frame, x, y) {

    //Это не совсем новые: они деленые на размеры спрайта, т.к. координаты
    //героя не домножаются на размер спрайта
    var newCoords = { x: frame.x / SPRITE_SIZE + x, y: frame.y / SPRITE_SIZE + y };
    if (!game.isInBorders(newCoords.x, newCoords.y)) { return; }
    // if ()

    if (spellToCast.type == "Projectile") {
        //Строить линию до рамки и проверять не пересекает ли она что-то непроходимое

        if (!canCast(game.hero.x, game.hero.y, newCoords.x, newCoords.y)) {
            frame.tint = 0xff1144;
            return;
        }

    }

    if (!game.map.objects[newCoords.y][newCoords.x].passable) {
        frame.tint = 0xff1144;
        return;
    }

    if (game.getDistance({ x: game.hero.x, y: game.hero.y }, newCoords) >= spellToCast.rng) {
        frame.tint = 0xff1144;
        return;
    }

    frame.tint = 0xffffff;
    frame.x += x * SPRITE_SIZE;
    frame.y += y * SPRITE_SIZE;

}

function canCast(xStart, yStart, xEnd, yEnd) {
    var l = Math.max(Math.abs(xEnd - xStart) + 1, Math.abs(yEnd - yStart) + 1);
    var x = xStart;
    var y = yStart;

    for (var i = 0; i < l; i++) {
        x += (xEnd - xStart) / l;
        y += (yEnd - yStart) / l;
        //console.log(Math.round(x) - xStart, Math.round(y) - yStart);
        //console.log(!(game.isInBorders(Math.round(x), Math.round(y)) && game.map.objects[Math.round(y)][Math.round(x)].passable));
        if (game.getActor(Math.round(x), Math.round(y)) === undefined) {
            if (!(game.isInBorders(Math.round(x), Math.round(y)) && game.map.objects[Math.round(y)][Math.round(x)].passable)) {
                //console.log("K");
                return false;
            }
        }
    }

    return true;
}

function castSpell(spell) {

    if (isCasting) return;
    spellToCast = spell;

    if (game.hero.stats.mp < spellToCast.manaCost) {
        myManaBar.flash(0xff2e2e);
        isCasting = false;
        return;
    }

    if (spell.type == "InstantCast") {
        game.hero.setNextAction(new CastAction(game.hero.x, game.hero.y, spellToCast));
        return;
    }

    //Если мы тут, то для каста заклинания нужно выбрать позицию каста
    isCasting = true;
    frame = phaserGame.add.sprite(game.hero.x * SPRITE_SIZE, game.hero.y * SPRITE_SIZE, 'Frame');
    phaserGame.camera.follow(frame, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
}

function onKeyDown(event) {


    // act on player input
    switch (event.keyCode) {
        case Phaser.Keyboard.LEFT:
            if (!isCasting) {
                game.hero.setNextAction(new WalkAction(-1, 0));
            } else {
                moveFrame(frame, -1, 0);
            }
            break;

        case Phaser.Keyboard.RIGHT:
            if (!isCasting) {
                game.hero.setNextAction(new WalkAction(1, 0));
            } else {
                moveFrame(frame, 1, 0);
            }
            break;

        case Phaser.Keyboard.UP:
            if (!isCasting) {
                game.hero.setNextAction(new WalkAction(0, -1));
            } else {
                moveFrame(frame, 0, -1);
            }
            break;

        case Phaser.Keyboard.DOWN:
            if (!isCasting) {
                game.hero.setNextAction(new WalkAction(0, 1));
            } else {
                moveFrame(frame, 0, 1);
            }
            break;

        case Phaser.Keyboard.ONE:
            castSpell(new FireBall(game.hero.stats));
            break;

        case Phaser.Keyboard.TWO:

            castSpell(new FrostBolt(game.hero.stats));
            break;

        case Phaser.Keyboard.THREE:
            castSpell(new LightningBolt(game.hero.stats));
            break;

        case Phaser.Keyboard.FOUR:
            castSpell(new PoisonSphere(game.hero.stats));
            break;

        case Phaser.Keyboard.FIVE:
            castSpell(new Vampirism(game.hero.stats));
            break;

        case Phaser.Keyboard.V:
            castSpell(new ForceExchange(game.hero.stats));
            break;

        case Phaser.Keyboard.C:
            castSpell(new Invisibility(game.hero.stats));
            break;

        case Phaser.Keyboard.X:
            castSpell(new Heal(game.hero.stats));
            break;

        case Phaser.Keyboard.Z:
            castSpell(new Teleportation(game.hero.stats));
            break;

        case Phaser.Keyboard.B:
            castSpell(new Push(game.hero.stats));
            break;

        case Phaser.Keyboard.ENTER:
            if (isCasting) {
                game.hero.setNextAction(new CastAction(frame.x / SPRITE_SIZE, frame.y / SPRITE_SIZE, spellToCast));
                isCasting = false;
                spellToCast = undefined;
                phaserGame.camera.follow(game.hero.sprite, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
                frame.kill();
            }
            break;

        case Phaser.Keyboard.ESC:
            if (isCasting) {
                isCasting = false;
                phaserGame.camera.follow(game.hero.sprite);
                frame.kill();
            }
            break;

        case Phaser.Keyboard.SPACEBAR:
            game.hero.setNextAction(new RestAction());
            break;

    }

    game.updateMobs();
}