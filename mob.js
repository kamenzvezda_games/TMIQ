class Mob extends Actor {
    constructor(position, name, spriteName, behavior, stats, race, attack, spells) {
        super();
        this.x = position.x;
        this.y = position.y;
        this.dx;
        this.dy;
        this.name = name;
        this.spriteName = spriteName;
        this.sprite;
        this.nextAction = null;
        this.behavior = behavior;
        this.stats = stats;
        this.race = race;
        this.attack = attack;
        this.effect;
        this.spells = spells;
        this.hpBar;
        ////console.log(name);
        ////console.log(this.stats);

    }

    initHpBar() {
        var barConfig = { width: SPRITE_SIZE - 5, height: 5, x: this.x * SPRITE_SIZE + SPRITE_SIZE / 2, y: this.y * SPRITE_SIZE + 2, bg: { color: '#871646' }, bar: { color: '#e03c28' } };
        this.hpBar = new Bar(phaserGame, barConfig);
        this.hpBar.setFixedToCamera(false);
    }

    updateBar() {
        this.hpBar.setPosition(this.x * SPRITE_SIZE + SPRITE_SIZE / 2, this.y * SPRITE_SIZE + 2);
        this.hpBar.setPercent(this.stats.hp * 100 / this.stats.maxHp);
    }

    takeAttack(attack) {
        //Придумать другое название переменной
        var reducedAttack = attack.getReducedAttack(this.stats);
        this.stats.hp -= reducedAttack.dmg;
        if (attack.effs != null) {
            if (this.effects[attack.effs.desc] === undefined) {
                //console.log("Effects Undefined adding new");
                this.effects[attack.effs.desc] = reducedAttack.effs;
                //console.log(this.effects[attack.effs.desc]);
                return;
            }
            this.effects[attack.effs.desc].duration += reducedAttack.effs.duration;
            //console.log(this.effects[attack.effs.desc]);
            //   this.effects[attack.effs.desc].affect(this);
        }

        // this.flash(0xff3333);
    }

    performAction() {
        //  this.nextAction = this.behavior.behave(this);
        if (this.nextAction === null) return;
        this.nextAction.perform(this);
        this.nextAction = null;
    }

}