class Behavior {
    behave() {};
}

class SimpleBehavior extends Behavior {
    constructor() {
        super();
        this.dirX = 1;
    }

    //Хождение по горизонтали до встречи с непроходимым объектом
    behave(actor) {
        if (!actor.canOccupy(actor.x + this.dirX, actor.y))
            this.dirX *= -1;
        return new WalkAction(this.dirX, 0);
    }
}

class MobBehavior extends Behavior {
    behave(actor) {
        var xEnd = actor.x;
        var yEnd = actor.y;
        var temp = { x: 0, y: 0 };
        var walkTo = { x: 0, y: 0 };
        var xStart = actor.x;
        var yStart = actor.y;
        var x = xStart;
        var y = yStart;
        var minDistance = ROWS * COLS;
        var spell = null;
        var attack = null;

        attack = this.chooseBestAttack(actor, attack, actor.attack);
        if (actor.spells != undefined) {
            spell = this.chooseBestSpell(actor, spell, actor.spells);
        }

        // if(spell != null) {
        //     attack = spell.attack;
        // }

        for (var i = 0; i < game.actors.length; i++) {
            var tempX = game.actors[i].x;
            var tempY = game.actors[i].y;
            var distance = game.getDistance(actor, game.actors[i]);

            if (distance <= 8 && actor.isEnemy(tempX, tempY)) {
                if (this.canSee(actor, tempX, tempY, temp, attack) && minDistance > distance && distance != 0) {
                    xStart = game.actors[i].x;
                    yStart = game.actors[i].y;
                    walkTo.x = temp.x;
                    walkTo.y = temp.y;
                    minDistance = distance;
                }
            }
        }

        if (xStart === actor.x && yStart === actor.y) {
            return this.randomMove();
        } else {
            if (spell != null) {
                if (actor.stats.mp >= spell.manaCost) {
                    //console.log(actor.stats.energy, spell.energyCost);
                    if (this.canHit(actor, game.getActor(xStart, yStart), spell.attack)) {
                        //console.log("Cast " + spell.energyCost);
                        return new CastAction(xStart, yStart, spell);
                    }
                }
            } else {
                if (actor.name === "Mage") {
                    //console.log("Can't cast");
                }
            }

            if (this.canHit(actor, game.getActor(xStart, yStart), attack)) {
                if (actor.name === "Mage") {
                    //console.log("Hit");
                }
                return new AttackAction(xStart, yStart, xStart - xEnd, yStart - yEnd, attack);
            }
        }

        if (Math.abs(walkTo.x - xEnd) > 1 || Math.abs(walkTo.y - yEnd) > 1) {
            //console.log("Wrong move " + (walkTo.x - xEnd) + " " + (walkTo.y - yEnd));
        }

        //Чтобы не ходил по диагонали
        if (Math.abs(walkTo.x - xEnd) === 1 && Math.abs(walkTo.y - yEnd) === 1) {
            var xLen = Math.abs(xStart - xEnd);
            var yLen = Math.abs(yStart - yEnd);

            if (xLen <= yLen) {
                walkTo.x = xEnd;
            } else walkTo.y = yEnd;
        }
        return new WalkAction(walkTo.x - xEnd, walkTo.y - yEnd);
    }

    canHit(actor, enemy, attack) {
        if (actor.stats.energy < attack.energyCost) {
            return false;
        }
        if (actor.name === "Mage") {
            //console.log(attack.rng, game.getDistance(actor, enemy));
        }
        if (attack.rng === 1) {
            var notDiagonal = (enemy.x - actor.x === 0) || (enemy.y - actor.y === 0);
            return (notDiagonal && enemy.x - actor.x >= -1 && enemy.x - actor.x <= 1 && enemy.y - actor.y >= -1 && enemy.y - actor.y <= 1);
        }
        return (attack.rng >= game.getDistance(actor, enemy));
    }

    // Строится линия, по которой смотрит моб
    canSee(actor, xEnemy, yEnemy, temp, attack) {
        if (!game.getActor(xEnemy, yEnemy).isVisible()) {
            return false;
        }
        var stepAmount = Math.max(Math.abs(actor.x - xEnemy) + 1, Math.abs(actor.y - yEnemy) + 1);
        var x = xEnemy;
        var y = yEnemy;

        for (var i = 0; i < stepAmount - 1; i++) {
            x += (actor.x - xEnemy) / stepAmount;
            y += (actor.y - yEnemy) / stepAmount;

            temp.x = Math.round(x);
            temp.y = Math.round(y);

            if (!actor.canOccupy(temp.x, temp.y) && !this.canHit(actor, game.getActor(xEnemy, yEnemy), attack)) {
                return false;
            }
        }
        return true;
    }

    chooseBestAttack(actor, item, array) {
        item = array[0];
        for (var i = 1; i < array.length; i++) {
            if (array[i].rng > item.rng && actor.stats.energy >= 8) {
                item = array[i];
            }
        }
        return item;
    }

    chooseBestSpell(actor, item, array) {
        item = array[0];
        for (var i = 1; i < array.length; i++) {
            if (array[i].rng > item.rng && actor.stats.energy >= array[i].energyCost) {
                item = array[i];
            }
        }
        return item;
    }

    randomMove() {
        var rand = Math.random();

        if (rand < 0.25) {
            return new WalkAction(1, 0);
        }

        if (rand < 0.5) {
            return new WalkAction(-1, 0);
        }

        if (rand < 0.75) {
            return new WalkAction(0, -1);
        }

        if (rand < 1) {
            return new WalkAction(0, 1);
        }
    }
}