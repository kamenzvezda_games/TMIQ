class Actor {
    constructor() {
        this.x;
        this.y;
        this.dx;
        this.dy;
        this.name;
        this.spriteName;
        this.sprite;
        this.nextAction;
        this.stats;
        this.effects = [];
        this.visibility = true;
        this.attacker;
    }

    applyEffects() {
        for (var e in this.effects) {
            this.effects[e].affect(this)
        }
    }

    isVisible() {
        return this.visibility;
    }

    rest() {
        this.stats.energy += this.stats.spd;
    }

    canPerformAction() {
        if (this.nextAction === null) return false;
        return this.stats.energy >= this.nextAction.energyCost;
    }

    needsInput() {
        return this.nextAction === null;
    }

    setNextAction(action) {
        this.nextAction = action;
    }

    //Придумать другое название
    burnEnergy(energyToBurn) {
        //Burn it with fire!!!1!
        this.stats.energy -= energyToBurn;
    }

    flash(color) {
        var actor = this.sprite;
        actor.tint = color;
        (function() {
            setTimeout(() => { actor.tint = 0xffffff }, 200);
        }());
    }

    //Придумать другое название
    burnMana(manaToBurn) {
        this.stats.mp -= manaToBurn;
    }

    canOccupy(x, y) {
        return game.isInBorders(x, y) && game.map.objects[y][x].passable && game.getActor(x, y) == undefined;
    }

    performAction() {
        if (this.nextAction === null) return;
        this.nextAction.perform(this);
        this.nextAction = null;
    }

    isEnemy(x, y) {
        var possibleEnemy = game.getActor(x, y);

        if (possibleEnemy === undefined) {
            return false;
        }

        for (var i = 0; i < game.relations.length; i++) {
            if (this.race === game.relations[i].race) {
                // console.log(game.relations[i].race);
                return game.relations[i][possibleEnemy.race];
            }
        }
    }
}