class Statistics {

    constructor() {
        this.mobsKilled = [];
        this.levelsDiscovered = 1;
        this.points = 0;
    }

    mobKilled(mob) {
        if (this.mobsKilled[mob.name] === undefined) {
            this.mobsKilled[mob.name] = 1;
            this.points += mob.stats.statsSum;
            return;
        }
        this.mobsKilled[mob.name]++;
        this.points += mob.stats.statsSum;
    }

    levelDiscovered() {
        this.levelsDiscovered++;
        this.points += 10;
    }

    //Много хардкода и всяких непоняных штук, потому что я не знаю 
    //как работать со строками
    getStatistics() {
        var mobsKilledStats = "";
        var text = "                    YOU SCORED: " + this.points.toString() + " points.";
        text += "\nDiscovered " + this.levelsDiscovered + " stages";
        text += "\nAdvanced to level " + game.hero.stats.lvl + "\n";
        if (game.hero.attacker == game.hero.name) {
            text += "Died from the suicide\n\n";
        } else {
            text += "Died from the hand of " + game.hero.attacker + "\n\n";
        }
        text += "Kill count: \n"
        var columnsCounter = 0;
        for (var mob in this.mobsKilled) {
            if (this.mobsKilled[mob] == 1) {
                var mobName = mob.toString();
            } else {
                var mobName = mob.toString() + "s";
            }
            mobsKilledStats += this.mobsKilled[mob] + ' ' + mobName;
            columnsCounter++;
            if (columnsCounter == 3) {
                mobsKilledStats += '\n';
                columnsCounter = 0;
            } else {
                for (var i = 0; i < 20 - mob.length + this.mobsKilled[mob].toString().length; i++) {
                    mobsKilledStats += " ";
                }
            }
        }
        text += mobsKilledStats.toString();
        return text;
    }
}