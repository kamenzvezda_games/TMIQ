class Stats {
    constructor(st) {

        this.lvl = st.lvl;

        //Пока нет оружия, поэтому + 10, что бы мобы как будто
        //наносили урон оружием, потом убрать
        this.str = st.str + 10;
        this.agl = st.agl;
        this.vit = st.vit;
        this.spd = st.spd
        this.int = 10;
        this.con = 30;

        this.mp = this.int * 2 + this.con + 10;
        this.hp = Math.floor(this.str * this.vit * 0.5) + 20;
        this.energy = this.spd * 2 + this.vit + 10;

        this.exp = 0;
    }

    get maxMp() {
        return this.int * 2 + this.con + 10;
    }

    get statsSum() {
        return this.lvl * 2 + this.maxHp * 3 + this.vit + this.spd + this.str;
    }

    get maxHp() {
        return Math.floor(this.str * this.vit * 0.5) + 20;
    }

    get maxEnergy() {
        return this.energy = this.spd * 3 + this.vit + 10;
    }

    gainEnergy() {
        var finalEnergy = this.energy + this.spd + this.vit;
        finalEnergy > this.maxEnergy ? this.energy = this.maxEnergy : this.energy = finalEnergy;
    }

    lvlCost() {
        if (this.lvl + 1 < 12) {
            return Math.round(0.0068 * Math.pow(this.lvl, 3) - 0.06 * Math.pow(this.lvl, 2) + 17.1 * this.lvl + 639);
        } else {
            return Math.round(0.02 * Math.pow(this.lvl, 3) + 3.06 * Math.pow(this.lvl, 2) + 105.6 * this.lvl - 895);
        }
    }
}