class Action {
    constructor() {
        this.energyCost = 0;
    }
    perform() {};
}

class CastAction extends Action {
    constructor(x, y, spell) {
        super();
        this.x = x;
        this.y = y;
        this.energyCost = spell.energyCost;
        this.spell = spell;
    }

    perform(caster) {

        if (caster.stats.mp < this.spell.manaCost) return;

        var actorToCastOn = game.getActor(this.x, this.y);
        //console.log("Casting on " + actorToCastOn);
        if (actorToCastOn != undefined) {
            actorToCastOn.attacker = caster.name;
        }
        //Отрисовываем спелл вне зависимости от того, есть ли на месте 
        //каста противник
        this.spell.draw(caster, this);
        this.spell.cast(caster, this.x, this.y);

        caster.burnMana(this.spell.manaCost);
    }
}

class WalkAction extends Action {
    constructor(dx, dy) {
        super(); //Чтобы юзать this
        this.dx = dx;
        this.energyCost = 7;
        this.dy = dy;
    }

    perform(actor) {
        // ////console.log("In Perform Moving" + game.currentActorIndex);

        let newX = actor.x + this.dx;
        let newY = actor.y + this.dy;
        //////console.log("Perform x " + newX + " y " + newY);

        if (actor.isEnemy(newX, newY)) {
            // //console.log(actor.stats.energy);
            actor.setNextAction(new AttackAction(newX, newY, this.dx, this.dy, new MeleeAttack(actor.stats.str, null)));
            if (!actor.canPerformAction()) {
                actor.setNextAction(new RestAction());
            }
            actor.performAction();
        }

        if (actor.canOccupy(newX, newY)) {
            actor.x = newX;
            actor.y = newY;
            actor.sprite.x = actor.x * SPRITE_SIZE;
            actor.sprite.y = actor.y * SPRITE_SIZE;
            actor.stats.energy -= this.energyCost;
        }
    }
}

class RestAction extends Action {
    perform(actor) {
        actor.stats.energy += actor.stats.spd;
    }
}

class AttackAction extends Action {
    constructor(x, y, dx, dy, attack) {
        super();
        this.x = x;
        this.y = y;
        this.sprite;
        this.dir = { x: dx, y: dy };
        this.energyCost = 8;
        this.attack = attack;
    }

    perform(actor) {
        var enemy = game.getActor(this.x, this.y);

        if (enemy != undefined) { //Временно, костыль
            enemy.attacker = actor.name;
            enemy.takeAttack(this.attack);
            actor.burnEnergy(this.energyCost);
        } else {
            actor.setNextAction(new WalkAction(this.dir.x, this.dir.y));
            actor.performAction();
            return;
        }
        this.attack.draw(actor, { x: this.x, y: this.y });
    }


}