class Attack {
    constructor(damage, effects, range) {
        this.dmg = damage;
        this.effs = effects;
        this.rng = range;
        this.sprite;
    }

    static draw(src, dir, sprite) {
        this.sprite = sprite;
        //Спрайт нужно поставить на центр клеточки, что бы вращение было нормальным
        //Поэтому к координатам прибавляется половина размера спрайты
        //console.log(dir.x + " " + dir.y);
        this.sprite = phaserGame.add.sprite(src.x * SPRITE_SIZE + SPRITE_SIZE / 2,
            src.y * SPRITE_SIZE + SPRITE_SIZE / 2, this.sprite);

        var obj = game.map.objects[dir.y][dir.x].sprite;
        var objTo = { x: obj.x + SPRITE_SIZE / 2, y: obj.y + SPRITE_SIZE / 2 }

        //Установка точки, относ. которой будет вращаться спрайт на центр
        this.sprite.anchor.setTo(0.5, 0.5);
        phaserGame.physics.enable(this.sprite);
        this.sprite.rotation = phaserGame.physics.arcade.angleBetween(this.sprite, objTo);
        var distance = game.getDistance(this.sprite, objTo);
        //Если актор хочет атаковать себя (скорее всего это ГГ)
        //То не стоит юзать этот метод, потому что он работает не корректно
        if (distance != 0) {
            phaserGame.physics.arcade.moveToXY(this.sprite, objTo.x, objTo.y, 500, Math.floor(distance));
            this.sprite.lifespan = distance;
        } else {
            //Елси герой кастует на себя, то время жизни такое
            this.sprite.lifespan = 50;
        }
    }

    getReducedAttack(enemyStats) {
        return this;
    }
}


class RangedAttack extends Attack {
    constructor(damage, effects, sprite) {
        super();
        this.dmg = damage;
        this.effs = effects;
        this.rng = 5;
        this.sprite = sprite;
    }

    draw(src, dir) {
        var spr = this.sprite;
        //Спрайт нужно поставить на центр клеточки, что бы вращение было нормальным
        //Поэтому к координатам прибавляется половина размера спрайты
        //console.log(dir.x + " " + dir.y);
        this.sprite = phaserGame.add.sprite(src.x * SPRITE_SIZE + SPRITE_SIZE / 2,
            src.y * SPRITE_SIZE + SPRITE_SIZE / 2, this.sprite);

        var obj = game.map.objects[dir.y][dir.x].sprite;
        var objTo = { x: obj.x + SPRITE_SIZE / 2, y: obj.y + SPRITE_SIZE / 2 }

        //Установка точки, относ. которой будет вращаться спрайт на центр
        this.sprite.anchor.setTo(0.5, 0.5);
        phaserGame.physics.enable(this.sprite);
        this.sprite.rotation = phaserGame.physics.arcade.angleBetween(this.sprite, objTo);
        var distance = game.getDistance(this.sprite, objTo);
        //Если актор хочет атаковать себя (скорее всего это ГГ)
        //То не стоит юзать этот метод, потому что он работает не корректно
        if (distance != 0) {
            phaserGame.physics.arcade.moveToXY(this.sprite, objTo.x, objTo.y, 500, Math.floor(distance));
            this.sprite.lifespan = distance;
        } else {
            //Елси герой кастует на себя, то время жизни такое
            this.sprite.lifespan = 50;
        }
        this.sprite = spr;
    }

    /**
     * @memberOf MeleeAttack
     * @description Applies enemy resistances to attack Придумать более хорошее имя
     * @returns class Attack with reduced/higher characteristics
     * @param enemyStats class Stats - enemy stats, that will be applied to Attack
     * 
     */
    getReducedAttack(enemyStats) {
        //Сделать тут что-то более осмысленное, возможно в будущем свзяное
        //с броней и эффектами
        //this.dmg -= enemyStats.vit / 10;
        return this;
    }
}

class StoneAttack extends RangedAttack {
    constructor(damage, effects) {
        super();
        this.dmg = damage;
        this.effs = effects;
        this.rng = 5;
        this.sprite = "Rock";
    }
}

class WheelAttack extends RangedAttack {
    constructor(damage, effects) {
        super();
        this.dmg = damage;
        this.effs = effects;
        this.rng = 8;
        this.sprite = "Wheel";
    }
}

class MeleeAttack extends Attack {
    constructor(damage, effects) {
        super();
        this.dmg = damage;
        this.effs = effects;
        this.rng = 1;
        this.sprite = 'MeleeAttack';
    }

    draw(src, dir) {
        //Спрайт нужно поставить на центр клеточки, что бы вращение было нормальным
        //Поэтому к координатам прибавляется половина размера спрайты
        var coefficients = { x: (dir.x - src.x), y: (dir.y - src.y) };
        //console.log(src.x + " " + src.y);
        //console.log(dir.x + " " + dir.y);
        if (src.y >= (phaserGame.camera.y / SPRITE_SIZE + 8)) {
            return;
        }
        var spr = this.sprite;
        this.sprite = phaserGame.add.sprite(src.x * SPRITE_SIZE + SPRITE_SIZE / 2,
            src.y * SPRITE_SIZE + SPRITE_SIZE / 2, this.sprite);
        //Установка точки, относ. которой будет вращаться спрайт на центр
        this.sprite.anchor.setTo(0.5, 0.5);
        //Придумать потом формулу без 
        this.sprite.angle = (coefficients.x * 90 - 90) * (-coefficients.y == 0 ? 1 : -coefficients.y);

        if (this.sprite != undefined) {
            this.sprite.lifespan = 50;
        }
        this.sprite = spr;
    }

    /**
     * @memberOf MeleeAttack
     * @description Applies enemy resistances to attack Придумать более хорошее имя
     * @returns class Attack with reduced/higher characteristics
     * @param enemyStats class Stats - enemy stats, that will be applied to Attack
     * 
     */
    getReducedAttack(enemyStats) {
        //Сделать тут что-то более осмысленное, возможно в будущем свзяное
        //с броней и эффектами
        //this.dmg -= enemyStats.vit / 10;
        return this;
    }
}

class AreaAttack extends Attack {
    constructor(damage, effects, range, radius) {
        super();
        this.dmg = damage;
        this.effs = effects;
        this.rng = range;
        this.rad = radius;
    }

}

class BeamAttack extends Attack {
    constructor(damage, effects, range, length) {
        super();
        this.dmg = damage;
        this.effs = effects;
        this.rng = range;
        this.len = length;
    }
}