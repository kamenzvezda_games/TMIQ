class ForestMap extends Map {
    constructor() {
        super();
        this.objects = [];
        this.previousMaps = [];
        this.availablePositions = [];
        this.isPassable = false;
        this.warpPos = {
            y: "any",
            x: COLS - 1
        };
    }

    generateMap(width, height) {
        //число "островков" объектов
        var objectSizes = {
            groundSize: Math.floor(Math.random() * (width / 10)) + height / 25,
            stoneSize: Math.floor(Math.random() * (width / 20)) + height / 12,
            bushSize: Math.floor(Math.random() * (width / 20)) + height / 12
        };
        
        var objectDensities = {
            groundDensity: width * 10,
            stoneDensity: width * 5,
            bushDensity: width * 5
        };

        this.prepareMaps(width, height, objectSizes, objectDensities)

        //tint below
        // for (var i = 0; i < this.availablePositions.length; i++) {
        //     this.objects[this.availablePositions[i].y][this.availablePositions[i].x].sprite.tint = 0.2 * 0xffffff;
        // }

    }

    prepareMaps(width, height, objectSizes, objectDensities) {
        for (var y = 0; y < height; y++) {
            this.objects[y] = [];
            for (var x = 0; x < width; x++) {
                this.objects[y][x] = "Grass";
            }
        }
        var stoneMap = [];
        var bushMap = [];
        var dummyArray = [];
        for (var i = 0; i < this.objects.length; i++) {
            stoneMap[i] = this.objects[i].slice();
            bushMap[i] = this.objects[i].slice();
            dummyArray[i] = this.objects[i].slice();
        }
        //указываем, поверх каких объектов может находиться объект
        var stonePlaces = ["Ground", "Grass"];
        var bushPlaces = ["Grass"];
        var passableObjects = ["Grass", "Ground"];

        while (!this.isPassable) {
            this.previousMaps = [];
            for (var i = 0; i < this.objects.length; i++) {
                stoneMap[i] = dummyArray[i].slice();
                bushMap[i] = dummyArray[i].slice();
                this.objects[i] = dummyArray[i].slice();
            }
            this.generateForest(this.objects, null, null, width, height, objectSizes.groundSize, 6, objectDensities.stoneDensity, "Ground", "Grass");
            this.generateForest(stoneMap, stonePlaces, this.previousMaps, width, height, objectSizes.stoneSize, 6, objectDensities.stoneDensity, "Stone", "Grass");
            this.generateForest(bushMap, bushPlaces, this.previousMaps, width, height, objectSizes.bushSize, 6, objectDensities.bushDensity, "Bush", "Grass");
            this.getAvailablePositions(width, height, passableObjects);
        }
        this.placeTransferTiles(this.objects, "Ground", "Grass", width, height);
        this.placeTransferTiles(stoneMap, "Stone", "Grass", width, height);
        this.placeTransferTiles(bushMap, "Bush", "Grass", width, height);

        this.convertToTileMap(this.objects, width, height, true, true, this.previousMaps, "Grass");
        this.convertToTileMap(stoneMap, width, height, false, false, this.previousMaps, "Grass");
        this.convertToTileMap(bushMap, width, height, false, false, this.previousMaps, "Grass");

        this.copyObjectsToMap(width, height, stoneMap, this.objects, "Grass", false);
        this.copyObjectsToMap(width, height, bushMap, this.objects, "Grass", false);
    }

    //находим все доступные для перемещения точки
    getAvailablePositions(width, height, passableObjects) {
        var staringYCoordinates = [];
        //находим свободную клетку у левого края
        for (var y = 0; y < height; y++) {
            if (this.checkPlacingPossibility(y, 0, this.previousMaps, passableObjects))
                staringYCoordinates.push(y);
        }
        for (var i = 0; i < staringYCoordinates.length; i++) {
            this.availablePositions = [];
            this.floodFill(width, staringYCoordinates[i], 0, passableObjects);
            if (this.isPassable) break;
        }
    }

    //основной алгоритм проверуи проходимости карты (от левого края до правого)
    floodFill(width, y, x, passableObjects) {
        if (!this.checkPlacingPossibility(y, x, this.previousMaps, passableObjects))
            return;
        if (x === width - 1) this.isPassable = true;
        this.availablePositions.push({
            x: x,
            y: y
        });
        if (game.isInBorders(x - 1, y) && !this.checkMarkedPositions(y, x - 1, this.availablePositions))
            this.floodFill(width, y, x - 1, passableObjects);
        if (game.isInBorders(x + 1, y) && !this.checkMarkedPositions(y, x + 1, this.availablePositions))
            this.floodFill(width, y, x + 1, passableObjects);
        if (game.isInBorders(x, y - 1) && !this.checkMarkedPositions(y - 1, x, this.availablePositions))
            this.floodFill(width, y - 1, x, passableObjects);
        if (game.isInBorders(x, y + 1) && !this.checkMarkedPositions(y + 1, x, this.availablePositions))
            this.floodFill(width, y + 1, x, passableObjects);
        return;
    }

    //вспомогательный для floodFill метод, проверяющий уже посещенные точки
    checkMarkedPositions(y, x, arr) {
        return arr.filter(function (array) {
            return (array.x === x && array.y === y)
        }).length > 0;
    }

    //проверяем, можно ли поставить объект в определенную точку на карте
    //(исходя из objectNamePlaces, которые задаем в generateForest)
    checkPlacingPossibility(y, x, previousMaps, placeOn) {
        if(previousMaps === undefined)
            previousMaps = {length: 1};
        for (var map = 0; map < previousMaps.length; map++) {
            var spriteName = previousMaps[map][y][x];
            if (placeOn.length === 0 || placeOn === undefined)
                return true;
            for (var i = 0; i < placeOn.length; i++)
                spriteName = spriteName.replace(placeOn[i], '');
            if (spriteName != "")
                return false;
        }
        return true;
    }

    generateForest(map, placeOn, previousMaps, width, height, forestSize, correctionRate, density, mainType, secondaryType) {
        for (var i = 0; i < forestSize; i++) {
            var x = Math.floor(Math.random() * width);
            var y = Math.floor(Math.random() * height);
            if (map != this.objects && !this.checkPlacingPossibility(y, x, previousMaps, placeOn)) {
                i--;
                continue;
            }
            this.generateForestPoint(map, placeOn, previousMaps, x, y, width, height, mainType, density);
        }
        //можно сделать еще вызовы forestCorrection, тогда лес будет более гладким и ровным
        for (var i = 0; i < correctionRate; i++)
            this.forestCorrection(map, width, height, mainType, secondaryType);
        this.previousMaps.push(map);

    }

    //вспомогательный для генерации леса метод: создаем "лес" вокруг точки
    generateForestPoint(map, placeOn, previousMaps, x, y, width, height, forestObj, density) {
        var i = y;
        var j = x;
        for (var dens = 0; dens < density; dens++) {
            //чем меньше диапазон значений ниже, тем гуще будет лес вокруг точки
            //можно этот диапазон изменяемым в будущем
            var east = Math.floor(Math.random() * 7);
            var north = Math.floor(Math.random() * 7);
            var south = Math.floor(Math.random() * 7);
            var west = Math.floor(Math.random() * 7);
            if (north === 1 && i > 0 && (map === this.objects || this.checkPlacingPossibility(i - 1, j, previousMaps, placeOn))) {
                i--;
                map[i][j] = forestObj;
            }
            if (south === 1 && i < height - 1 && (map === this.objects || this.checkPlacingPossibility(i + 1, j, previousMaps, placeOn))) {
                i++;
                map[i][j] = forestObj;
            }
            if (east === 1 && j < width - 1 && (map === this.objects || this.checkPlacingPossibility(i, j + 1, previousMaps, placeOn))) {
                j++;
                map[i][j] = forestObj;
            }
            if (west === 1 && j > 0 && (map === this.objects || this.checkPlacingPossibility(i, j - 1, previousMaps, placeOn))) {
                j--;
                map[i][j] = forestObj;
            }
        }
    }

    forestCorrection(map, width, height, mainType, secondaryType) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                if(this.checkSurroundedTiles(map, y, x, mainType, secondaryType))
                map[y][x] = secondaryType 
                else map[y][x] = mainType;
            }
        }
    }
}